# 如何使用
## 导入数据
当首次使用这个程序时候，先解压 `doc` 目录下的 `数据库文件.zip`，将`SQL`文件导入数据库
> 应确保和 `DB.ts` 文件中定义的一致
> 开发时使用的 `MySQL` 版本是 `5.7.26`
> 在`src/lib目录下定义了 ts 文件`
```ts
const DB = new Sequelize('SMS', '123456', '123456', {
    host: 'localhost',
    dialect: 'mysql',
    logging: false
});
```

## 安装依赖`npm i`
> 你必须先安装相关依赖之后才能运行程序
> 开发时使用的 `nodejs` 版本是 `14.14.0`

## 启动程序
> 你可以使用 `ts-node` 启动 `src/index.ts`
> 你可以使用 `node` 启动 `dist/index.js`

## 浏览程序
在浏览器里输入 `localhost/home` 进入程序首页
> 端口定义在 `src/index.ts` 的末尾，默认监听的是`80`端口，可以不用输入