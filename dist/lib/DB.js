"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DB = void 0;
var sequelize_1 = require("sequelize");
// 方法 2: 分别传递参数 (其它数据库)
var DB = new sequelize_1.Sequelize('SMS', '123456', '123456', {
    host: 'localhost',
    dialect: 'mysql',
    logging: false
});
exports.DB = DB;
DB.authenticate().then(function () {
    console.log("数据库链接成功");
}).catch(function () {
    console.warn("数据库连接失败");
});
module.exports = { DB: DB };
