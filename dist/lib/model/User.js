"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Profession = exports.UserRole = exports.UserStatus = exports.User = void 0;
var DB_1 = require("../DB");
var sequelize_1 = require("sequelize");
// 用户
exports.User = DB_1.DB.define('user', {
    id: {
        type: sequelize_1.DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        comment: '主键',
    },
    name: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
        comment: '用户名',
        validate: {
            notEmpty: true
        }
    },
    truename: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: true,
        comment: '真实姓名',
        validate: {
            notEmpty: true
        }
    },
    avatar: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: true,
        comment: '头像地址',
    },
    email: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
        comment: '电子邮箱',
        validate: {
            isEmail: true
        }
    },
    tel: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
        comment: '手机号',
        validate: {
            len: [6, 20]
        }
    },
    password: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
        comment: '密码',
        validate: {
            len: [6, 20]
        }
    },
    studentId: {
        type: sequelize_1.DataTypes.STRING,
        comment: '学号'
    },
    age: {
        type: sequelize_1.DataTypes.TINYINT,
        comment: '年龄'
    },
});
// 状态
exports.UserStatus = DB_1.DB.define('user_status', {
    id: {
        type: sequelize_1.DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        comment: '主键',
    },
    name: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
        comment: '状态名字'
    }
});
// 角色
exports.UserRole = DB_1.DB.define('user_role', {
    id: {
        type: sequelize_1.DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        comment: '主键',
    },
    name: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
        comment: '角色名字'
    }
});
// 专业
exports.Profession = DB_1.DB.define('profession', {
    id: {
        type: sequelize_1.DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        comment: '主键',
    },
    name: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
        comment: '专业名称'
    }
});
exports.User.belongsTo(exports.UserStatus);
exports.UserStatus.hasMany(exports.User, { foreignKey: { defaultValue: 1 }, onDelete: 'SET NULL' });
exports.User.belongsTo(exports.UserRole);
exports.UserRole.hasMany(exports.User, { foreignKey: { defaultValue: 1 }, onDelete: 'SET NULL' });
exports.User.belongsTo(exports.Profession);
exports.Profession.hasMany(exports.User);
exports.User.sync({ alter: true });
