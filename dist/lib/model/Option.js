"use strict";
var DB_1 = require("../DB");
var sequelize_1 = require("sequelize");
var Option = DB_1.DB.define('option', {
    id: {
        type: sequelize_1.DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    content: {
        type: sequelize_1.DataTypes.TEXT,
        allowNull: false
    },
    answer: {
        type: sequelize_1.DataTypes.BLOB,
        allowNull: false
    }
});
module.exports = { Option: Option };
