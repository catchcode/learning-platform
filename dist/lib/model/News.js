"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.News = void 0;
var DB_1 = require("../DB");
var sequelize_1 = require("sequelize");
var News = DB_1.DB.define('news', {
    id: {
        type: sequelize_1.DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    content: {
        type: sequelize_1.DataTypes.TEXT,
        allowNull: false,
        comment: '通知内容，html格式'
    },
    title: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
        comment: '通知标题'
    },
    createLocaleTime: {
        type: sequelize_1.DataTypes.VIRTUAL,
        get: function () {
            // @ts-ignore
            return new Date(this.createdAt).toLocaleString();
        },
        comment: '虚拟字段，返回格式化后的时间'
    }
});
exports.News = News;
