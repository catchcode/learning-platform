"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExerciseList = exports.ExerciseOption = exports.ExerciseTopic = void 0;
var DB_1 = require("../DB");
var sequelize_1 = require("sequelize");
// 练习
var ExerciseList = DB_1.DB.define('exercise_list', {
    id: {
        type: sequelize_1.DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    title: {
        type: sequelize_1.DataTypes.TEXT,
        allowNull: false,
        comment: '练习标题',
    },
    level: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
        defaultValue: '正常',
        comment: '题目等级',
    },
    sum: {
        type: sequelize_1.DataTypes.VIRTUAL,
        comment: '总分',
        get: function () {
            // @ts-ignore
            return this.exercise_topics.reduce(function (sum, cur) {
                return parseInt(cur.toJSON().grade) + sum;
            }, 0);
        }
    }
});
exports.ExerciseList = ExerciseList;
// 题目
var ExerciseTopic = DB_1.DB.define('exercise_topic', {
    id: {
        type: sequelize_1.DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    title: {
        type: sequelize_1.DataTypes.TEXT,
        allowNull: false,
        comment: '题目标题'
    },
    grade: {
        type: sequelize_1.DataTypes.INTEGER,
        allowNull: false,
        comment: '分数'
    },
    answerCount: {
        type: sequelize_1.DataTypes.VIRTUAL,
        get: function () {
            // @ts-ignore
            return this.options ? this.options.filter(function (e) { return !!e.answer; }).length : 0;
        },
        comment: "正确答案的数量"
    }
});
exports.ExerciseTopic = ExerciseTopic;
// 选项
var ExerciseOption = DB_1.DB.define('exercise_option', {
    id: {
        type: sequelize_1.DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    content: {
        type: sequelize_1.DataTypes.TEXT,
        allowNull: false,
        comment: '选项内容'
    },
    answer: {
        type: sequelize_1.DataTypes.TINYINT,
        allowNull: false,
        comment: '答案，正确是1，错误是0'
    }
});
exports.ExerciseOption = ExerciseOption;
ExerciseTopic.belongsTo(ExerciseList);
ExerciseList.hasMany(ExerciseTopic);
ExerciseOption.belongsTo(ExerciseTopic);
ExerciseTopic.hasMany(ExerciseOption);
