"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Chapter = exports.Course = void 0;
var DB_1 = require("../DB");
var sequelize_1 = require("sequelize");
// 定义一个栏目的模型
var Course = DB_1.DB.define("course", {
    id: {
        type: sequelize_1.DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false
    },
    url: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false
    },
    img: {
        type: sequelize_1.DataTypes.TEXT,
        allowNull: false
    },
    description: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false
    }
});
exports.Course = Course;
// 章节
var Chapter = DB_1.DB.define("chapter", {
    id: {
        type: sequelize_1.DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    title: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
    },
    // 菜鸟教程URL
    url: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false
    },
    content: {
        type: sequelize_1.DataTypes.TEXT({ length: 'medium' }),
        allowNull: false
    },
    courseID: {
        type: sequelize_1.DataTypes.INTEGER
    }
});
exports.Chapter = Chapter;
Course.hasMany(Chapter, { foreignKey: "courseID" });
