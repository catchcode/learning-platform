"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StudyRecordType = exports.StudyRecord = void 0;
var DB_1 = require("../lib/DB");
var sequelize_1 = require("sequelize");
var StudyRecord = DB_1.DB.define('study_record', {
    id: {
        type: sequelize_1.DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    ip: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
        validate: {
            isIP: true
        },
        comment: '客户IP地址'
    },
    grade: {
        type: sequelize_1.DataTypes.INTEGER,
        comment: '分数'
    },
    createLocaleTime: {
        type: sequelize_1.DataTypes.VIRTUAL,
        get: function () {
            // @ts-ignore
            return new Date(this.createdAt).toLocaleString();
        }
    }
});
exports.StudyRecord = StudyRecord;
var StudyRecordType = DB_1.DB.define('study_record_type', {
    id: {
        type: sequelize_1.DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
        comment: '类型的名字'
    }
});
exports.StudyRecordType = StudyRecordType;
StudyRecordType.hasMany(StudyRecord, {});
StudyRecord.belongsTo(StudyRecordType);
var User_1 = require("./User");
User_1.User.hasMany(StudyRecord);
StudyRecord.belongsTo(User_1.User);
