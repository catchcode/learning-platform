"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.VideoSection = exports.VideoList = exports.VideoLevel = exports.VideoSubject = void 0;
var sequelize_1 = require("sequelize");
var DB_1 = require("../lib/DB");
// 学科
exports.VideoSubject = DB_1.DB.define("video_subject", {
    id: {
        type: sequelize_1.DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
        comment: '学科名字'
    },
});
// 难度
exports.VideoLevel = DB_1.DB.define("video_level", {
    id: {
        type: sequelize_1.DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        comment: '主键'
    },
    name: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
        comment: '难度名称'
    }
});
// 视频列表
exports.VideoList = DB_1.DB.define('video_list', {
    id: {
        type: sequelize_1.DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    title: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
        comment: '视频标题'
    },
    banner: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
        comment: '视频主要展示图片'
    },
    num: {
        type: sequelize_1.DataTypes.INTEGER,
        allowNull: false,
        comment: '学习人数'
    },
    lead: {
        type: sequelize_1.DataTypes.TEXT,
        allowNull: false,
        defaultValue: '无',
        comment: '下载本章视频你能学到什么？'
    }
});
// 视频（视频章节）
exports.VideoSection = DB_1.DB.define("video_section", {
    id: {
        type: sequelize_1.DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
        comment: '章节名字'
    },
    url: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
        comment: '视频播放地址'
    },
});
exports.VideoSubject.hasMany(exports.VideoList);
exports.VideoList.belongsTo(exports.VideoSubject);
exports.VideoLevel.hasMany(exports.VideoList);
exports.VideoList.belongsTo(exports.VideoLevel);
exports.VideoList.hasMany(exports.VideoSection);
exports.VideoSection.belongsTo(exports.VideoList);
