"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Log = void 0;
var DB_1 = require("../lib/DB");
var sequelize_1 = require("sequelize");
var User_1 = require("./User");
exports.Log = DB_1.DB.define('log', {
    id: {
        type: sequelize_1.DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    ms: {
        type: sequelize_1.DataTypes.INTEGER,
        allowNull: false,
        comment: '处理这个请求花费的时间（毫秒）'
    },
    ip: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
        validate: {
            isIP: true
        },
        comment: '客户IP地址'
    },
    method: {
        type: sequelize_1.DataTypes.ENUM('GET', 'POST', 'HEAD', 'PUT', 'DELETE', 'CONNECT', 'OPTIONS', 'TRACE'),
        allowNull: false,
        comment: "请求方法"
    },
    href: {
        type: sequelize_1.DataTypes.TEXT,
        allowNull: false,
        comment: '获取完整的请求URL，包括 protocol，host 和 url。',
    },
    query: {
        type: sequelize_1.DataTypes.TEXT,
        comment: "获取解析的查询字符串, 当没有查询字符串时，返回一个空对象。[json]"
    },
    body: {
        type: sequelize_1.DataTypes.TEXT,
        comment: "请求体[json]"
    },
    header: {
        type: sequelize_1.DataTypes.TEXT,
        comment: "请求头对象[json]"
    },
    raw_request: {
        type: sequelize_1.DataTypes.TEXT,
        comment: "未处理的请求对象[json]",
    },
    raw_response: {
        type: sequelize_1.DataTypes.TEXT,
        comment: "未处理的响应对象[json]"
    }
});
exports.Log.belongsTo(User_1.User);
User_1.User.hasMany(exports.Log);
