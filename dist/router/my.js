"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
/*
 * @Descripttion: 个人中心页面
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-04-30 20:52:57
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-05-01 07:50:02
 */
var koa_router_1 = __importDefault(require("koa-router"));
var router = new koa_router_1.default();
// 模型
var User_1 = require("../model/User");
var online_1 = __importDefault(require("../middleware/online"));
/** 个人中心 */
router.get('/my', online_1.default, function (ctx) { return __awaiter(void 0, void 0, void 0, function () {
    var professions;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, User_1.Profession.findAll({ limit: 100 })];
            case 1:
                professions = (_a.sent()).map(function (e) { return e.toJSON(); });
                return [4 /*yield*/, ctx.render('subpage/my', { professions: professions })];
            case 2:
                _a.sent();
                return [2 /*return*/];
        }
    });
}); });
/** 修改 */
router.post('/my', online_1.default, function (ctx) { return __awaiter(void 0, void 0, void 0, function () {
    var user, e_1, professions;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 3, , 6]);
                return [4 /*yield*/, User_1.User.findOne({ where: { id: ctx.session.user.id, password: ctx.request.body.password }, include: [User_1.UserRole, User_1.UserStatus, User_1.Profession] })];
            case 1:
                user = (_a.sent());
                if (user == null) {
                    throw new Error('密码错误');
                }
                user.set('truename', ctx.request.body.truename);
                user.set('age', ctx.request.body.age);
                user.set('studentId', ctx.request.body.studentId);
                user.set('professionId', ctx.request.body.professionId);
                return [4 /*yield*/, user.save()];
            case 2:
                _a.sent();
                // 更新session
                ctx.session.user = user === null || user === void 0 ? void 0 : user.toJSON();
                ctx.redirect('/my');
                return [3 /*break*/, 6];
            case 3:
                e_1 = _a.sent();
                return [4 /*yield*/, User_1.Profession.findAll({ limit: 100 })];
            case 4:
                professions = (_a.sent()).map(function (e) { return e.toJSON(); });
                return [4 /*yield*/, ctx.render('subpage/my', { professions: professions, danger: [e_1.message] })];
            case 5:
                _a.sent();
                return [3 /*break*/, 6];
            case 6: return [2 /*return*/];
        }
    });
}); });
module.exports = router;
