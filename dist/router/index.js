"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var fs_1 = __importDefault(require("fs"));
var path_1 = __importDefault(require("path"));
function default_1(app) {
    var _a;
    var dir = fs_1.default.opendirSync(__dirname);
    var name = null;
    while (name = (_a = dir.readSync()) === null || _a === void 0 ? void 0 : _a.name) {
        if (name != 'index.js' && name != 'other.js') {
            var router = require(path_1.default.join(__dirname, name));
            try {
                app.use(router.routes()).use(router.allowedMethods());
            }
            catch (e) {
                console.error("\u52A0\u8F7D\u8DEF\u7531 " + name + " \u65F6\u629B\u51FA\uFF1A" + e.message);
            }
        }
    }
    var other = require('./other');
    app.use(other.routes()).use(other.allowedMethods());
}
exports.default = default_1;
