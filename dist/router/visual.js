"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// @ts-nocheck
/*
 * @Descripttion: 数据可视化
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-05-01 18:11:03
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-05-09 13:30:08
 */
var koa_router_1 = __importDefault(require("koa-router"));
var DB_1 = require("../lib/DB");
var Log_1 = require("../model/Log");
var sequelize_1 = require("sequelize");
var router = new koa_router_1.default();
router.get("/visual", function (ctx, next) { return __awaiter(void 0, void 0, void 0, function () {
    var percentage_distribution, active_time, questionnaire, questionnaire_profession, questionnaire_position, questionnaire_learning_style;
    var _a;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0: return [4 /*yield*/, DB_1.DB.query('select sum(study_records.grade) as grade from study_records GROUP BY study_records.userId')
                    // 返回分数数组 [12,20,30] 
                    .then(function (_a) {
                    var data = _a[0];
                    // @ts-ignore
                    return data.map(function (e) { return parseInt(e.grade); });
                })
                    // 区间
                    .then(function (data) {
                    return [
                        data.filter(function (e) { return e < 100; }).length,
                        data.filter(function (e) { return e > 100 && e < 300; }).length,
                        data.filter(function (e) { return e > 300 && e < 500; }).length,
                        data.filter(function (e) { return e > 500; }).length,
                    ];
                })
                    .then(function (data) {
                    return JSON.stringify(data);
                })
                // 活跃时间段
            ];
            case 1:
                percentage_distribution = _b.sent();
                return [4 /*yield*/, DB_1.DB.query("SELECT * from `logs` where `logs`.href like '%/video?%videoSectionId%' or `logs`.href like '%/chapters?courseID=%'").then(function (_a) {
                        var data = _a[0];
                        var res = [];
                        var _loop_1 = function (i) {
                            // @ts-ignore
                            res.push(data.filter(function (e) { return new Date(e.createdAt).getHours() == i; }).length + 3);
                        };
                        for (var i = 0; i < 24; i++) {
                            _loop_1(i);
                        }
                        return res;
                    }).then(function (data) {
                        return JSON.stringify(data);
                    })
                    // 问卷调查
                ];
            case 2:
                active_time = _b.sent();
                return [4 /*yield*/, Log_1.Log.findAll({
                        where: {
                            href: (_a = {},
                                _a[sequelize_1.Op.like] = 'http://%/questionnaire',
                                _a),
                            method: 'POST'
                        }
                    }).then(function (data) {
                        return data.map(function (e) { return JSON.parse(e.body); });
                    })];
            case 3:
                questionnaire = _b.sent();
                console.log(questionnaire);
                questionnaire_profession = JSON.stringify([
                    {
                        value: questionnaire.filter(function (e) { return e.profession == 'JY'; }).length,
                        name: '计算机应用技术'
                    },
                    {
                        name: '物联网',
                        value: questionnaire.filter(function (e) { return e.profession == 'JY'; }).length,
                    }, {
                        name: '云计算',
                        value: questionnaire.filter(function (e) { return e.profession == 'YJS'; }).length,
                    }, {
                        name: '会计',
                        value: questionnaire.filter(function (e) { return e.profession == 'HJ'; }).length,
                    }
                ]);
                questionnaire_position = JSON.stringify([
                    {
                        value: questionnaire.filter(function (e) { return e.position.indexOf('CEO') > -1; }).length,
                        name: '项目总经理'
                    },
                    {
                        value: questionnaire.filter(function (e) { return e.position.indexOf('WM') > -1; }).length,
                        name: '产品主管'
                    },
                    {
                        value: questionnaire.filter(function (e) { return e.position.indexOf('UI') > -1; }).length,
                        name: 'UI设计师'
                    }, {
                        value: questionnaire.filter(function (e) { return e.position.indexOf('AD') > -1; }).length,
                        name: '美工'
                    }, {
                        value: questionnaire.filter(function (e) { return e.position.indexOf('WF') > -1; }).length,
                        name: '前端开发工程师'
                    }, {
                        value: questionnaire.filter(function (e) { return e.position.indexOf('YW') > -1; }).length,
                        name: '产品运维工程师'
                    }, {
                        value: questionnaire.filter(function (e) { return e.position.indexOf('FX') > -1; }).length,
                        name: '数据分析师'
                    }, {
                        value: questionnaire.filter(function (e) { return e.position.indexOf('SJ') > -1; }).length,
                        name: '设计师'
                    }
                ]);
                questionnaire_learning_style = JSON.stringify([{
                        value: questionnaire.filter(function (e) { return e.learningStyle.indexOf('video') > -1; }).length,
                        name: "观看视频",
                    },
                    {
                        value: questionnaire.filter(function (e) { return e.learningStyle.indexOf('document') > -1; }).length,
                        name: "查阅文档",
                    },
                    {
                        value: questionnaire.filter(function (e) { return e.learningStyle.indexOf('topic') > -1; }).length,
                        name: "刷题",
                    },
                    {
                        value: questionnaire.filter(function (e) { return e.learningStyle.indexOf('other') > -1; }).length,
                        name: "其他",
                    }
                ]);
                console.log(questionnaire_profession);
                console.log(questionnaire_learning_style);
                console.log(questionnaire_position);
                ctx.render('subpage/visual', { percentage_distribution: percentage_distribution, active_time: active_time, questionnaire_profession: questionnaire_profession, questionnaire_position: questionnaire_position, questionnaire_learning_style: questionnaire_learning_style });
                return [4 /*yield*/, next()];
            case 4:
                _b.sent();
                return [2 /*return*/];
        }
    });
}); });
module.exports = router;
