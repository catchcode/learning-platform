"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
/*
 * @Descripttion: 视频页面
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-04-30 20:53:22
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-05-01 12:54:21
 */
var koa_router_1 = __importDefault(require("koa-router"));
var router = new koa_router_1.default();
// 模型
var Video_1 = require("../model/Video");
/** 视频列表 */
router.get('/videolist', function (ctx) { return __awaiter(void 0, void 0, void 0, function () {
    var limit, page, currentPage, offset, levels, _a, subjectId, levelId, where, count, countPage, subjects, videos;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                limit = 12;
                page = parseInt(ctx.request.query.page) || 1;
                currentPage = page - 1;
                offset = currentPage * limit;
                return [4 /*yield*/, Video_1.VideoLevel.findAll()];
            case 1:
                levels = (_b.sent()).map(function (e) { return e.toJSON(); });
                _a = ctx.query, subjectId = _a.subjectId, levelId = _a.levelId;
                where = { videolevelId: levelId, videoSubjectId: subjectId };
                if (levelId == undefined || levelId == '' || levels.find(function (e) { return e.id == levelId.trim(); }) == undefined) {
                    delete where.videolevelId;
                }
                if (subjectId == undefined || subjectId == '' || typeof parseInt(subjectId) != 'number') {
                    delete where.videoSubjectId;
                }
                return [4 /*yield*/, Video_1.VideoList.count({ where: where })];
            case 2:
                count = _b.sent();
                countPage = Math.ceil(count / limit);
                return [4 /*yield*/, Video_1.VideoSubject.findAll()];
            case 3:
                subjects = (_b.sent()).map(function (e) { return e.toJSON(); });
                return [4 /*yield*/, Video_1.VideoList.findAll({ where: where, include: [Video_1.VideoLevel, Video_1.VideoSubject], limit: limit, offset: offset })];
            case 4:
                videos = (_b.sent()).map(function (e) { return e.toJSON(); });
                return [4 /*yield*/, ctx.render('subpage/videolist', {
                        subjects: subjects,
                        videos: videos,
                        subjectId: subjectId,
                        levelId: levelId,
                        levels: levels,
                        countPage: countPage,
                        page: page
                    })];
            case 5:
                _b.sent();
                return [2 /*return*/];
        }
    });
}); });
/** 视频 */
router.get('/video', function (ctx, next) { return __awaiter(void 0, void 0, void 0, function () {
    var _a, videoListId, videoSectionId, videoSections, videoSection, _b, videoSubjectId, recommends;
    var _c, _d;
    return __generator(this, function (_e) {
        switch (_e.label) {
            case 0:
                _a = ctx.query, videoListId = _a.videoListId, videoSectionId = _a.videoSectionId;
                if (videoListId == undefined || typeof parseInt(videoListId) != 'number') {
                    next();
                    return [2 /*return*/];
                }
                return [4 /*yield*/, Video_1.VideoSection.findAll({ where: { videoListId: videoListId }, include: Video_1.VideoList })];
            case 1:
                videoSections = (_e.sent()).map(function (e) { return e.toJSON(); });
                if (!!videoSectionId) return [3 /*break*/, 2];
                _b = videoSections[0];
                return [3 /*break*/, 4];
            case 2: return [4 /*yield*/, Video_1.VideoSection.findOne({ where: { id: videoSectionId }, include: Video_1.VideoList })];
            case 3:
                _b = (_c = (_e.sent())) === null || _c === void 0 ? void 0 : _c.toJSON();
                _e.label = 4;
            case 4:
                videoSection = _b;
                videoSubjectId = (_d = videoSections[0].video_list['videoSubjectId']) !== null && _d !== void 0 ? _d : undefined;
                return [4 /*yield*/, Video_1.VideoList.findAll({ where: { videoSubjectId: videoSubjectId }, limit: 8 })];
            case 5:
                recommends = (_e.sent()).map(function (e) { return e.toJSON(); });
                return [4 /*yield*/, ctx.render('common/video', {
                        videoSections: videoSections,
                        videoSection: videoSection,
                        videoListId: videoListId,
                        recommends: recommends
                    })];
            case 6:
                _e.sent();
                return [2 /*return*/];
        }
    });
}); });
module.exports = router;
