"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// @ts-nocheck
/*
 * @Descripttion: 学习记录页面
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-04-30 20:52:18
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-05-08 21:19:59
 */
var koa_router_1 = __importDefault(require("koa-router"));
var router = new koa_router_1.default();
// 模型
var Study_1 = require("../model/Study");
// 中间件
var online_1 = __importDefault(require("../middleware/online"));
var DB_1 = require("../lib/DB");
var User_1 = require("../model/User");
/** 学习记录 */
router.get('/credit', online_1.default, function (ctx, next) { return __awaiter(void 0, void 0, void 0, function () {
    var studys, user_studys, sum, recommend_user, offset, recommend_user_id, e_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, Study_1.StudyRecord.findAll({ where: { userId: ctx.session.user.id }, limit: 50, include: Study_1.StudyRecordType, order: [['createdAt', 'DESC']] })];
            case 1:
                studys = (_a.sent()).map(function (e) { return e.toJSON(); });
                return [4 /*yield*/, Study_1.StudyRecord.findAll({ where: { userId: ctx.session.user.id } })];
            case 2:
                user_studys = (_a.sent()).map(function (e) { return e.toJSON(); });
                sum = user_studys.reduce(function (s, cur) {
                    return s + cur.grade;
                }, 0);
                recommend_user = {};
                _a.label = 3;
            case 3:
                _a.trys.push([3, 6, , 7]);
                offset = 50;
                return [4 /*yield*/, DB_1.DB.query("select userId from ( select sum(grade) as sum ,userId from study_records GROUP BY study_records.userId ) as A where A.sum BETWEEN :min and :max and A.userId != :self ORDER BY A.sum limit 0,1", {
                        // @ts-ignore
                        replacements: { min: sum - offset, max: sum + offset, self: ctx.session.user.id }
                    }).then(function (_a) {
                        var data = _a[0][0];
                        // @ts-ignore
                        return data.userId || null;
                    })];
            case 4:
                recommend_user_id = _a.sent();
                return [4 /*yield*/, User_1.User.findOne({
                        where: {
                            id: recommend_user_id
                        },
                        include: [
                            {
                                model: User_1.Profession,
                                as: 'profession'
                            }
                        ]
                    }).then(function (res) {
                        if (res == null) {
                            throw new Error();
                        }
                        else {
                            return res.toJSON();
                        }
                        // 求学习天数
                    }).then(function (res) {
                        var learning_days = Date.now() - new Date(res.createdAt).getTime();
                        res.learning_days = Math.ceil(learning_days / 1000 / 60 / 60 / 24);
                        return res;
                    })];
            case 5:
                recommend_user = _a.sent();
                return [3 /*break*/, 7];
            case 6:
                e_1 = _a.sent();
                recommend_user = false;
                return [3 /*break*/, 7];
            case 7:
                console.log(recommend_user);
                return [4 /*yield*/, ctx.render('subpage/credit', { studys: studys, sum: sum, recommend_user: recommend_user })];
            case 8:
                _a.sent();
                return [4 /*yield*/, next()];
            case 9:
                _a.sent();
                return [2 /*return*/];
        }
    });
}); });
module.exports = router;
