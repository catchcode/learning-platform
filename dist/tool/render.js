"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var fs_1 = __importDefault(require("fs"));
var path_1 = __importDefault(require("path"));
var vue_server_renderer_1 = require("vue-server-renderer");
// 第 2 步：创建一个 renderer
var renderer = vue_server_renderer_1.createRenderer({
    template: fs_1.default.readFileSync(path_1.default.resolve('src', 'views', 'default', 'template.html'), 'utf-8')
});
exports.default = renderer.renderToString;
