"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var puppeteer_1 = __importDefault(require("puppeteer"));
var Video_1 = require("../model/Video");
var DOMAIN = 'http://video.mobiletrain.org';
var browser = puppeteer_1.default.launch({ headless: false });
(function () { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        return [2 /*return*/];
    });
}); })();
/** 爬取视频章节 */
function spiderSection() {
    return __awaiter(this, void 0, void 0, function () {
        var page, hrefs, _i, hrefs_1, href, _loop_1, state_1;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, browser];
                case 1: return [4 /*yield*/, (_a.sent()).newPage()];
                case 2:
                    page = _a.sent();
                    return [4 /*yield*/, Video_1.VideoSection.destroy({ where: {} })];
                case 3:
                    _a.sent();
                    return [4 /*yield*/, page.goto('http://video.mobiletrain.org/courses/')];
                case 4:
                    _a.sent();
                    return [4 /*yield*/, page.$$eval('body > section.filter-nav > div > div.clearfix.filter-type1 > div.fl.clearfix > a.fl', function (elements) {
                            return elements.map(function (e) {
                                return e.getAttribute('href');
                            });
                        })];
                case 5:
                    hrefs = _a.sent();
                    hrefs.shift(); // 弹出第一个全部
                    _i = 0, hrefs_1 = hrefs;
                    _a.label = 6;
                case 6:
                    if (!(_i < hrefs_1.length)) return [3 /*break*/, 12];
                    href = hrefs_1[_i];
                    return [4 /*yield*/, page.goto(new URL(href, DOMAIN).toString())];
                case 7:
                    _a.sent();
                    _loop_1 = function () {
                        var list, video_page, MAX_COUNT, next_url, e_1;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0: return [4 /*yield*/, page.$$eval('body > section.video-list > div > ul > li', function (elements) {
                                        return elements.map(function (e) {
                                            var _a, _b, _c, _d;
                                            return {
                                                title: (_b = (_a = e.querySelector('.video-title')) === null || _a === void 0 ? void 0 : _a.textContent) === null || _b === void 0 ? void 0 : _b.trim(),
                                                href: (_d = (_c = e.querySelector('a')) === null || _c === void 0 ? void 0 : _c.getAttribute('href')) === null || _d === void 0 ? void 0 : _d.trim()
                                            };
                                        });
                                    })];
                                case 1:
                                    list = _b.sent();
                                    video_page = function () {
                                        var _a, _b, _c;
                                        return __awaiter(this, void 0, void 0, function () {
                                            var _i, list_1, iterator, video_page_1, videoListContent, sections, _d, sections_1, section, videoList;
                                            return __generator(this, function (_e) {
                                                switch (_e.label) {
                                                    case 0:
                                                        _i = 0, list_1 = list;
                                                        _e.label = 1;
                                                    case 1:
                                                        if (!(_i < list_1.length)) return [3 /*break*/, 15];
                                                        iterator = list_1[_i];
                                                        return [4 /*yield*/, browser];
                                                    case 2: return [4 /*yield*/, (_e.sent()).newPage()];
                                                    case 3:
                                                        video_page_1 = _e.sent();
                                                        return [4 /*yield*/, video_page_1.goto(new URL((_a = iterator.href) !== null && _a !== void 0 ? _a : '', DOMAIN).toString())];
                                                    case 4:
                                                        _e.sent();
                                                        return [4 /*yield*/, video_page_1.$('body > div.section2.warp.clearfix > div.fl.section2-left > div.section2-left-nav.clearfix > a:nth-child(2)')];
                                                    case 5: 
                                                    // 点击[课程目录]
                                                    return [4 /*yield*/, ((_b = (_e.sent())) === null || _b === void 0 ? void 0 : _b.click())
                                                        /** 视频的标题，查找ID用 */
                                                    ];
                                                    case 6:
                                                        // 点击[课程目录]
                                                        _e.sent();
                                                        return [4 /*yield*/, video_page_1.$eval('body > div.section1 > div.bread-nav.warp > a:nth-child(5)', function (e) { var _a; return (_a = e.textContent) === null || _a === void 0 ? void 0 : _a.trim(); })];
                                                    case 7:
                                                        videoListContent = _e.sent();
                                                        return [4 /*yield*/, video_page_1.$$eval('body > div.section2.warp.clearfix > div.fl.section2-left > div.section2-left-conts > div.course-list.active > ul > li > ul > li.clearfix.j-url-list', function (elements) {
                                                                return elements.map(function (e) {
                                                                    var _a, _b, _c, _d;
                                                                    return ({
                                                                        name: (_b = (_a = e.querySelector('p')) === null || _a === void 0 ? void 0 : _a.textContent) === null || _b === void 0 ? void 0 : _b.trim(),
                                                                        url: (_d = (_c = e.querySelector('a')) === null || _c === void 0 ? void 0 : _c.getAttribute('data-url')) === null || _d === void 0 ? void 0 : _d.trim(),
                                                                        videoListId: null,
                                                                    });
                                                                });
                                                            })];
                                                    case 8:
                                                        sections = _e.sent();
                                                        return [4 /*yield*/, video_page_1.close()
                                                            // 补充 ID 属性
                                                        ];
                                                    case 9:
                                                        _e.sent();
                                                        _d = 0, sections_1 = sections;
                                                        _e.label = 10;
                                                    case 10:
                                                        if (!(_d < sections_1.length)) return [3 /*break*/, 13];
                                                        section = sections_1[_d];
                                                        return [4 /*yield*/, Video_1.VideoList.findOne({ where: { title: videoListContent } })];
                                                    case 11:
                                                        videoList = (_c = (_e.sent())) === null || _c === void 0 ? void 0 : _c.toJSON();
                                                        section.videoListId = videoList.id;
                                                        _e.label = 12;
                                                    case 12:
                                                        _d++;
                                                        return [3 /*break*/, 10];
                                                    case 13:
                                                        Video_1.VideoSection.bulkCreate(sections);
                                                        _e.label = 14;
                                                    case 14:
                                                        _i++;
                                                        return [3 /*break*/, 1];
                                                    case 15: return [2 /*return*/];
                                                }
                                            });
                                        });
                                    };
                                    MAX_COUNT = 20;
                                    return [4 /*yield*/, browser];
                                case 2: return [4 /*yield*/, (_b.sent()).pages()];
                                case 3:
                                    if (!((_b.sent()).length > MAX_COUNT)) return [3 /*break*/, 5];
                                    return [4 /*yield*/, video_page()];
                                case 4:
                                    _b.sent();
                                    return [3 /*break*/, 6];
                                case 5:
                                    video_page();
                                    _b.label = 6;
                                case 6:
                                    _b.trys.push([6, 11, , 12]);
                                    return [4 /*yield*/, page.$eval('body > section.video-list > div > div > a:last-child', function (e) {
                                            return e.getAttribute('href');
                                        })];
                                case 7:
                                    next_url = _b.sent();
                                    if (!(next_url == null)) return [3 /*break*/, 8];
                                    return [2 /*return*/, "break"];
                                case 8: return [4 /*yield*/, page.goto(next_url)];
                                case 9:
                                    _b.sent();
                                    _b.label = 10;
                                case 10: return [3 /*break*/, 12];
                                case 11:
                                    e_1 = _b.sent();
                                    return [2 /*return*/, "break"];
                                case 12: return [2 /*return*/];
                            }
                        });
                    };
                    _a.label = 8;
                case 8: return [5 /*yield**/, _loop_1()];
                case 9:
                    state_1 = _a.sent();
                    if (state_1 === "break")
                        return [3 /*break*/, 11];
                    _a.label = 10;
                case 10:
                    if (true) return [3 /*break*/, 8];
                    _a.label = 11;
                case 11:
                    _i++;
                    return [3 /*break*/, 6];
                case 12: return [2 /*return*/];
            }
        });
    });
}
/** 爬取视频列表 */
function spiderList() {
    return __awaiter(this, void 0, void 0, function () {
        var page, hrefs, _loop_2, _i, hrefs_2, href;
        var _this = this;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, browser];
                case 1: return [4 /*yield*/, (_a.sent()).newPage()];
                case 2:
                    page = _a.sent();
                    return [4 /*yield*/, Video_1.VideoList.destroy({ where: {} })];
                case 3:
                    _a.sent();
                    return [4 /*yield*/, page.goto('http://video.mobiletrain.org/courses/')];
                case 4:
                    _a.sent();
                    return [4 /*yield*/, page.$$eval('body > section.filter-nav > div > div.clearfix.filter-type1 > div.fl.clearfix > a.fl', function (elements) {
                            return elements.map(function (e) {
                                return e.getAttribute('href');
                            });
                        })];
                case 5:
                    hrefs = _a.sent();
                    hrefs.shift(); // 弹出第一个全部
                    _loop_2 = function (href) {
                        var active_content, subject, _loop_3, state_2;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0: return [4 /*yield*/, page.goto(new URL(href, DOMAIN).toString())];
                                case 1:
                                    _b.sent();
                                    return [4 /*yield*/, page.$eval('body > section.filter-nav > div > div.clearfix.filter-type1 > div.fl.clearfix > a.fl.active', function (e) { return e.textContent; })];
                                case 2:
                                    active_content = _b.sent();
                                    return [4 /*yield*/, Video_1.VideoSubject.findOne({ where: { name: active_content === null || active_content === void 0 ? void 0 : active_content.trim() } })];
                                case 3:
                                    subject = _b.sent();
                                    _loop_3 = function () {
                                        var list, save, MAX_COUNT, next_url, e_2;
                                        return __generator(this, function (_c) {
                                            switch (_c.label) {
                                                case 0: return [4 /*yield*/, page.$$eval('body > section.video-list > div > ul > li', function (elements) {
                                                        return elements.map(function (e) {
                                                            var _a, _b, _c, _d, _e, _f, _g, _h, _j;
                                                            return {
                                                                banner: (_b = (_a = e.querySelector('.video-pic')) === null || _a === void 0 ? void 0 : _a.getAttribute('src')) === null || _b === void 0 ? void 0 : _b.trim(),
                                                                num: parseInt((_c = e.querySelector('.video-study-num')) === null || _c === void 0 ? void 0 : _c.textContent),
                                                                title: (_e = (_d = e.querySelector('.video-title')) === null || _d === void 0 ? void 0 : _d.textContent) === null || _e === void 0 ? void 0 : _e.trim(),
                                                                // 等级文字，稍后改成ID
                                                                _level_content: (_g = (_f = e.querySelector('.video-bot-box > .video-level')) === null || _f === void 0 ? void 0 : _f.textContent) === null || _g === void 0 ? void 0 : _g.trim(),
                                                                _href: (_j = (_h = e.querySelector('a')) === null || _h === void 0 ? void 0 : _h.getAttribute('href')) === null || _j === void 0 ? void 0 : _j.trim(),
                                                                videoSubjectId: null,
                                                                videoLevelId: null,
                                                                lead: ''
                                                            };
                                                        });
                                                    })
                                                    // 补上外键信息
                                                ];
                                                case 1:
                                                    list = _c.sent();
                                                    save = function () { return __awaiter(_this, void 0, void 0, function () {
                                                        var tmp_page, _i, list_2, iterator, level, lead;
                                                        var _a, _b, _c;
                                                        return __generator(this, function (_d) {
                                                            switch (_d.label) {
                                                                case 0: return [4 /*yield*/, browser];
                                                                case 1: return [4 /*yield*/, (_d.sent()).newPage()];
                                                                case 2:
                                                                    tmp_page = _d.sent();
                                                                    _i = 0, list_2 = list;
                                                                    _d.label = 3;
                                                                case 3:
                                                                    if (!(_i < list_2.length)) return [3 /*break*/, 8];
                                                                    iterator = list_2[_i];
                                                                    return [4 /*yield*/, Video_1.VideoLevel.findOne({ where: { name: iterator._level_content } })];
                                                                case 4:
                                                                    level = (_a = (_d.sent())) === null || _a === void 0 ? void 0 : _a.toJSON();
                                                                    return [4 /*yield*/, tmp_page.goto(new URL((_b = iterator._href) !== null && _b !== void 0 ? _b : '', DOMAIN).toString())];
                                                                case 5:
                                                                    _d.sent();
                                                                    return [4 /*yield*/, tmp_page.$eval('body > div.section1 > div.clearfix.warp > div.fr.video-describe > div.video-lead > p', function (e) {
                                                                            var _a;
                                                                            return (_a = e.textContent) === null || _a === void 0 ? void 0 : _a.trim();
                                                                        })];
                                                                case 6:
                                                                    lead = _d.sent();
                                                                    iterator.videoLevelId = level.id;
                                                                    iterator.videoSubjectId = subject.id;
                                                                    // 如果可行，应该使用 new URL 拼接
                                                                    iterator.banner = new URL((_c = iterator.banner) !== null && _c !== void 0 ? _c : '', DOMAIN).toString();
                                                                    iterator.lead = lead;
                                                                    delete iterator._level_content;
                                                                    delete iterator._href;
                                                                    _d.label = 7;
                                                                case 7:
                                                                    _i++;
                                                                    return [3 /*break*/, 3];
                                                                case 8: return [4 /*yield*/, tmp_page.close()];
                                                                case 9:
                                                                    _d.sent();
                                                                    return [4 /*yield*/, Video_1.VideoList.bulkCreate(list)];
                                                                case 10:
                                                                    _d.sent();
                                                                    return [2 /*return*/];
                                                            }
                                                        });
                                                    }); };
                                                    MAX_COUNT = 20;
                                                    return [4 /*yield*/, browser];
                                                case 2: return [4 /*yield*/, (_c.sent()).pages()];
                                                case 3:
                                                    if (!((_c.sent()).length > MAX_COUNT)) return [3 /*break*/, 5];
                                                    return [4 /*yield*/, save()];
                                                case 4:
                                                    _c.sent();
                                                    return [3 /*break*/, 6];
                                                case 5:
                                                    save();
                                                    _c.label = 6;
                                                case 6:
                                                    _c.trys.push([6, 11, , 12]);
                                                    return [4 /*yield*/, page.$eval('body > section.video-list > div > div > a:last-child', function (e) {
                                                            return e.getAttribute('href');
                                                        })];
                                                case 7:
                                                    next_url = _c.sent();
                                                    if (!(next_url == null)) return [3 /*break*/, 8];
                                                    return [2 /*return*/, "break"];
                                                case 8: return [4 /*yield*/, page.goto(next_url)];
                                                case 9:
                                                    _c.sent();
                                                    _c.label = 10;
                                                case 10: return [3 /*break*/, 12];
                                                case 11:
                                                    e_2 = _c.sent();
                                                    return [2 /*return*/, "break"];
                                                case 12: return [2 /*return*/];
                                            }
                                        });
                                    };
                                    _b.label = 4;
                                case 4: return [5 /*yield**/, _loop_3()];
                                case 5:
                                    state_2 = _b.sent();
                                    if (state_2 === "break")
                                        return [3 /*break*/, 7];
                                    _b.label = 6;
                                case 6:
                                    if (true) return [3 /*break*/, 4];
                                    _b.label = 7;
                                case 7: return [2 /*return*/];
                            }
                        });
                    };
                    _i = 0, hrefs_2 = hrefs;
                    _a.label = 6;
                case 6:
                    if (!(_i < hrefs_2.length)) return [3 /*break*/, 9];
                    href = hrefs_2[_i];
                    return [5 /*yield**/, _loop_2(href)];
                case 7:
                    _a.sent();
                    _a.label = 8;
                case 8:
                    _i++;
                    return [3 /*break*/, 6];
                case 9: return [2 /*return*/];
            }
        });
    });
}
/** 爬取难度 */
function spiderLevel() {
    return __awaiter(this, void 0, void 0, function () {
        var page, levels;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, browser];
                case 1: return [4 /*yield*/, (_a.sent()).newPage()];
                case 2:
                    page = _a.sent();
                    return [4 /*yield*/, page.goto('http://video.mobiletrain.org/courses/')];
                case 3:
                    _a.sent();
                    return [4 /*yield*/, page.$$eval('body > section.filter-nav > div > div.clearfix.filter-type2 > div.fl.clearfix > a.fl', function (elements) {
                            return elements.map(function (e) { var _a; return (_a = e.textContent) === null || _a === void 0 ? void 0 : _a.trim(); });
                        })];
                case 4:
                    levels = (_a.sent()).map(function (e) { return ({
                        name: e
                    }); });
                    return [4 /*yield*/, Video_1.VideoLevel.destroy({ truncate: true })];
                case 5:
                    _a.sent();
                    return [4 /*yield*/, Video_1.VideoLevel.bulkCreate(levels)];
                case 6:
                    _a.sent();
                    return [2 /*return*/];
            }
        });
    });
}
/** 爬取学科 */
function spiderSubject() {
    return __awaiter(this, void 0, void 0, function () {
        var page, subjects;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, browser];
                case 1: return [4 /*yield*/, (_a.sent()).newPage()];
                case 2:
                    page = _a.sent();
                    return [4 /*yield*/, page.goto('http://video.mobiletrain.org/courses/')];
                case 3:
                    _a.sent();
                    return [4 /*yield*/, page.$$eval('body > section.filter-nav > div > div.clearfix.filter-type1 > div.fl.clearfix > a.fl', function (elements) {
                            return elements.map(function (e) { var _a; return (_a = e.textContent) === null || _a === void 0 ? void 0 : _a.trim(); });
                        })];
                case 4:
                    subjects = (_a.sent()).map(function (e) { return ({
                        name: e
                    }); });
                    return [4 /*yield*/, Video_1.VideoSubject.destroy({ truncate: true })];
                case 5:
                    _a.sent();
                    return [4 /*yield*/, Video_1.VideoSubject.bulkCreate(subjects)];
                case 6:
                    _a.sent();
                    return [2 /*return*/];
            }
        });
    });
}
