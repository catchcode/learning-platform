"use strict";
/*
 * @Descripttion: 爬取千锋教育里的视频-- http://video.mobiletrain.org/courses/
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-04-20 13:41:24
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-04-20 15:05:54
 */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Extract = exports.Load = exports.browser = void 0;
var puppeteer_1 = __importDefault(require("puppeteer"));
exports.browser = puppeteer_1.default.launch();
// 加载器
var Load = /** @class */ (function () {
    // 构造函数
    function Load(page) {
        /** url 列队 */
        this.queues = [];
        /** 处理速度(毫秒) */
        this.speed = 1000;
        /** 提取器列表 */
        this.extracts = [];
        this.page = page;
    }
    /**
     * @name: 自由如风
     * @access: private
     * @version: 1.0
     * @param {*}
     * @return {*}
     * @description 创建一个加载器对象，并分配一个 page 对象
     */
    Load.create = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0: return [4 /*yield*/, exports.browser];
                    case 1:
                        _b.sent();
                        _a = Load.bind;
                        return [4 /*yield*/, exports.browser];
                    case 2: return [4 /*yield*/, (_b.sent()).newPage()];
                    case 3: return [2 /*return*/, new (_a.apply(Load, [void 0, _b.sent()]))()];
                }
            });
        });
    };
    /**
     * @name: 自由如风
     * @access: private
     * @version: 1.0
     * @param {string} url
     * @return {void}
     * @description 添加一个URL到加载器里，稍后，它将被处理
     */
    Load.prototype.addUrl = function (url) {
        if (!(url instanceof Array)) {
            url = [url];
        }
        this.queues = this.queues.concat(url);
    };
    /**
     * @name: 自由如风
     * @access: private
     * @version: 1.0
     * @param {*}
     * @return {*}
     * @description 添加提取器以获取数据
     */
    Load.prototype.addExtract = function (extract) {
        extract = extract instanceof Array ? extract : [extract];
        this.extracts = this.extracts.concat(extract);
    };
    /**
     * @name: 自由如风
     * @access: private
     * @version: 1.0
     * @param {*}
     * @return {*}
     * @description 加载那些 URL
     */
    Load.prototype.load = function () {
        return __awaiter(this, void 0, void 0, function () {
            var url, _i, _a, extract;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        if (!!!this.queues.length) return [3 /*break*/, 6];
                        url = this.queues.pop();
                        return [4 /*yield*/, this.page.goto(url)];
                    case 1:
                        _b.sent();
                        _i = 0, _a = this.extracts;
                        _b.label = 2;
                    case 2:
                        if (!(_i < _a.length)) return [3 /*break*/, 5];
                        extract = _a[_i];
                        return [4 /*yield*/, new extract(this.page, this).extract()];
                    case 3:
                        _b.sent();
                        _b.label = 4;
                    case 4:
                        _i++;
                        return [3 /*break*/, 2];
                    case 5: return [3 /*break*/, 0];
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    return Load;
}());
exports.Load = Load;
/** 提取器 */
var Extract = /** @class */ (function () {
    function Extract(page, load) {
        this.page = page;
        this.load = load;
    }
    /**
     * @name: 自由如风
     * @access: private
     * @version: 1.0
     * @param {*}
     * @return {*}
     * @description 加载器会等待这个方法返回
     */
    Extract.prototype.extract = function () {
    };
    return Extract;
}());
exports.Extract = Extract;
/* 存储器 */
var Storage = /** @class */ (function () {
    function Storage() {
    }
    return Storage;
}());
