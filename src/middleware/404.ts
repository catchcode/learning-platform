/*
 * @Descripttion: 主动渲染 404 页面
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-04-30 21:41:57
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-04-30 21:42:06
 */
import Koa from 'koa'
export default async function (ctx: Koa.ParameterizedContext<Koa.DefaultState, Koa.DefaultContext, any>, next: Function) {
    ctx.render('common/404')
    await next();
}