import Koa from 'koa'
export default async function (ctx: Koa.ParameterizedContext<Koa.DefaultState, Koa.DefaultContext, any>, next: Function) {
    if (ctx.session!.user == undefined) {
        ctx.render('static/login', { danger: ['该操作需要登录，请先登录'] })
    } else {
        await next();
    }
}