/*
 * @Descripttion: 应用级中间件，负责记录请求以及响应日志
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-05-01 15:14:51
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-05-09 13:20:48
 */
import Koa from 'koa'
import { Log } from '../model/Log'
export default async function (ctx: Koa.ParameterizedContext<Koa.DefaultState, Koa.DefaultContext, any>, next: Function) {
    const start_time = Date.now();
    await next();
    const end_time = Date.now();

    /** 返回符合数据库的json数据 */
    function get_json(obj: Object) {
        const json = JSON.stringify(obj);
        if (json == '{}' || json == undefined) {
            return null;
        } else {
            return json
        }
    }
    console.log(ctx.request.url);

    Log.create({
        ms: end_time - start_time,
        userId: ctx.session?.user?.id,
        ip: ctx.request.ip,
        method: ctx.request.method,
        href: ctx.request.href,
        query: get_json(ctx.request.query),
        body: get_json(ctx.request.body),
        header: get_json(ctx.request.header),
        raw_request: get_json(ctx.request),
        raw_response: get_json(ctx.response),
    })
}