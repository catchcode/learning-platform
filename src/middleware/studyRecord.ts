// 使用中间件保存学习记录 类似日志 区分  GET POST 
import Koa from 'koa'
import { StudyRecord, StudyRecordType } from '../model/Study'
export async function studyRecord(ctx: Koa.ParameterizedContext<Koa.DefaultState, Koa.DefaultContext, any>, next: Function) {
    if (ctx.session?.user) {
        //if ((Date.now() - ctx.session.study ?? 0) < 10 * 1000) { return } else { ctx.session.study = Date.now() }
        switch (ctx.request.URL.pathname.substr(1)) {
            case 'chapters':
                StudyRecord.create({
                    ip: ctx.ip,
                    userId: ctx.session.user.id,
                    studyRecordTypeId: 1,
                    grade: 1,
                })
                break;
            case 'video':
                StudyRecord.create({
                    ip: ctx.ip,
                    userId: ctx.session.user.id,
                    studyRecordTypeId: 2,
                    grade: 2,
                })
                break;
        }

    }
    await next()
}