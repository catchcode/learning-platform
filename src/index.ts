import Koa from "koa";
const app = new Koa();
import path from "path";
import koa_static from "koa-static";
import router from "./router/index";
import log from './middleware/log';

// 异常处理
app.use(async (ctx, next) => {
  try {
    await next();
  } catch (e) {
    console.error(e.message);
    ctx.render('common/error', { danger: [e.message] })
  }
})

// session
import session from "koa-session";

const CONFIG = {
  key: "koa:sess", //cookie key (default is koa:sess) 默认
  maxAge: 12 * 60 * 60 * 1000, // cookie 的过期时间 maxAge in ms (default is 1 days)             【需要修改】
  overwrite: true, //是否可以 overwrite (默认 default true)
  httpOnly: true, //cookie 是否只有服务器端可以访问 httpOnly or not (default true)
  signed: false, //签名默认 true
  rolling: false, //在每次请求时强行设置 cookie，这将重置 cookie 过期时间（默认：false） 【需要修改 跟下面的二选一】
  renew: true, //等快要到期时重置 ☆前提是你此次请求的session还没有过期 然后在发请求的时候会给你重置为新的  【需要修改】
};
app.use(session(CONFIG, app));

// 静态资源服务器
app.use(koa_static(path.resolve("public")));

// 日志
app.use(log)

// art-template 模板引擎
const render = require("koa-art-template");
render(app, {
  root: path.resolve("src", "views"),
  extname: ".art",
  debug: process.env.NODE_ENV !== "production",
  imports: {}
});





// 学习记录
import { studyRecord } from './middleware/studyRecord'
app.use(studyRecord)

// 模板访问session
const template = require('art-template');
app.use(async (ctx, next) => {
  template.defaults.imports.session = ctx.session || null;
  await next();
})

import bodyParser from 'koa-bodyparser';
app.use(bodyParser());
// 路由
router(app);

// 404 错误
app.use((ctx) => {
  if (ctx.status == 404) {
    ctx.render('common/404', { info: ['页面丢失了'] })
  }
})

app.listen(80);
