import { Sequelize } from "sequelize";

// 方法 2: 分别传递参数 (其它数据库)
const DB = new Sequelize('SMS', '123456', '123456', {
    host: 'localhost',
    dialect: 'mysql',
    logging: false
});

DB.authenticate().then(() => {
    console.log("数据库链接成功");
}).catch(() => {
    console.warn("数据库连接失败")
})
export { DB }
module.exports = { DB }