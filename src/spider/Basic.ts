/*
 * @Descripttion: 爬取千锋教育里的视频-- http://video.mobiletrain.org/courses/
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-04-20 13:41:24
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-04-20 15:05:54
 */

import puppeteer from 'puppeteer'


export const browser = puppeteer.launch()


export type ExtractClass = { new(page: puppeteer.Page, load: Load): Extract }
// 加载器
export class Load {
    /** url 列队 */
    public queues: string[] = [];
    /** 处理速度(毫秒) */
    public speed: number = 1000
    /** 提取器列表 */
    public extracts: ExtractClass[] = []
    /** puppeteer 页面实例 */
    public page: puppeteer.Page
    // 构造函数
    constructor(page: puppeteer.Page) {
        this.page = page
    }
    /**
     * @name: 自由如风
     * @access: private
     * @version: 1.0
     * @param {*}
     * @return {*}
     * @description 创建一个加载器对象，并分配一个 page 对象
     */
    static async create() {
        await browser
        return new Load(await (await browser).newPage())
    }
    /**
     * @name: 自由如风
     * @access: private
     * @version: 1.0
     * @param {string} url
     * @return {void}
     * @description 添加一个URL到加载器里，稍后，它将被处理
     */
    addUrl(url: string | string[]): void {
        if (!(url instanceof Array)) {
            url = [url]
        }
        this.queues = this.queues.concat(url)
    }
    /**
     * @name: 自由如风
     * @access: private
     * @version: 1.0
     * @param {*}
     * @return {*}
     * @description 添加提取器以获取数据
     */
    addExtract(extract: ExtractClass | ExtractClass[]): void {
        extract = extract instanceof Array ? extract : [extract]
        this.extracts = this.extracts.concat(extract)
    }

    /**
     * @name: 自由如风
     * @access: private
     * @version: 1.0
     * @param {*}
     * @return {*}
     * @description 加载那些 URL
     */
    private async load() {
        while (!!this.queues.length) {
            const url: string = <string>this.queues.pop();

            await this.page.goto(url)

            for (const extract of this.extracts) {
                await new extract(this.page, this).extract()
            }
        }
    }
}

/** 提取器 */
export abstract class Extract {
    constructor(public page: puppeteer.Page, public load: Load) {

    }
    /**
     * @name: 自由如风
     * @access: private
     * @version: 1.0
     * @param {*}
     * @return {*}
     * @description 加载器会等待这个方法返回
     */
    extract(): Promise<any> | any {

    }
}


/* 存储器 */
class Storage {
    constructor() {

    }

}

