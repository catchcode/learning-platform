import puppeteer from 'puppeteer'

import { VideoSubject, VideoLevel, VideoList, VideoSection } from '../model/Video'


const DOMAIN = 'http://video.mobiletrain.org'

const browser = puppeteer.launch({ headless: false });

(async () => {
    // 爬取学科
    // spiderSubject()
    // 爬取难度
    // spiderLevel()
    // 爬取视频
    // spiderList()
    // 爬取章节
    //spiderSection()

})()

/** 爬取视频章节 */
async function spiderSection() {
    const page = await (await browser).newPage()

    await VideoSection.destroy({ where: {} })

    await page.goto('http://video.mobiletrain.org/courses/')

    const hrefs = await page.$$eval('body > section.filter-nav > div > div.clearfix.filter-type1 > div.fl.clearfix > a.fl', (elements) => {
        return elements.map(e => {
            return e.getAttribute('href')
        })
    })

    hrefs.shift() // 弹出第一个全部

    for (const href of hrefs) {
        await page.goto(new URL(<string>href, DOMAIN).toString());

        do {

            const list = await page.$$eval('body > section.video-list > div > ul > li', (elements) => {
                return elements.map(e => {
                    return {
                        title: e.querySelector('.video-title')?.textContent?.trim(),
                        href: e.querySelector('a')?.getAttribute('href')?.trim()
                    }
                })
            });

            const video_page = async function () {
                for (const iterator of list) {

                    const video_page = await (await browser).newPage()
                    await video_page.goto(new URL(iterator.href ?? '', DOMAIN).toString());

                    // 点击[课程目录]
                    await (await video_page.$('body > div.section2.warp.clearfix > div.fl.section2-left > div.section2-left-nav.clearfix > a:nth-child(2)'))?.click()

                    /** 视频的标题，查找ID用 */
                    const videoListContent = await video_page.$eval('body > div.section1 > div.bread-nav.warp > a:nth-child(5)', (e) => e.textContent?.trim())

                    const sections = await video_page.$$eval('body > div.section2.warp.clearfix > div.fl.section2-left > div.section2-left-conts > div.course-list.active > ul > li > ul > li.clearfix.j-url-list', (elements) => {
                        return elements.map(e => ({
                            name: e.querySelector('p')?.textContent?.trim(),
                            url: e.querySelector('a')?.getAttribute('data-url')?.trim(),
                            videoListId: null,
                        }))
                    })
                    await video_page.close()
                    // 补充 ID 属性
                    for (const section of sections) {
                        const videoList: any = (await VideoList.findOne({ where: { title: videoListContent } }))?.toJSON()
                        section.videoListId = videoList.id
                    }

                    VideoSection.bulkCreate(sections)
                }
            }

            // 如果子页太多则等待，否则异步
            const MAX_COUNT = 20
            if ((await (await browser).pages()).length > MAX_COUNT) {
                await video_page()
            } else {
                video_page()
            }
            // 尝试爬取下一页
            try {
                const next_url = await page.$eval('body > section.video-list > div > div > a:last-child', (e) => {
                    return e.getAttribute('href')
                })

                if (next_url == null) {
                    break
                } else {
                    await page.goto(next_url)
                }
            } catch (e) {
                break;
            }

        } while (true)

    }
}

/** 爬取视频列表 */
async function spiderList() {
    const page = await (await browser).newPage()


    await VideoList.destroy({ where: {} })

    await page.goto('http://video.mobiletrain.org/courses/')

    const hrefs = await page.$$eval('body > section.filter-nav > div > div.clearfix.filter-type1 > div.fl.clearfix > a.fl', (elements) => {
        return elements.map(e => {
            return e.getAttribute('href')
        })
    })

    hrefs.shift() // 弹出第一个全部

    for (const href of hrefs) {
        await page.goto(new URL(<string>href, DOMAIN).toString());

        const active_content = await page.$eval('body > section.filter-nav > div > div.clearfix.filter-type1 > div.fl.clearfix > a.fl.active', (e) => e.textContent)
        const subject: any = await VideoSubject.findOne({ where: { name: active_content?.trim() } })



        do {

            const list = await page.$$eval('body > section.video-list > div > ul > li', (elements) => {
                return elements.map(e => {
                    return {
                        banner: e.querySelector('.video-pic')?.getAttribute('src')?.trim(),
                        num: parseInt(<string>e.querySelector('.video-study-num')?.textContent),
                        title: e.querySelector('.video-title')?.textContent?.trim(),
                        // 等级文字，稍后改成ID
                        _level_content: e.querySelector('.video-bot-box > .video-level')?.textContent?.trim(),
                        _href: e.querySelector('a')?.getAttribute('href')?.trim(),
                        videoSubjectId: null, // 学科ID
                        videoLevelId: null,
                        lead: ''
                    }
                })
            })
            // 补上外键信息
            const save = async () => {
                const tmp_page = await (await browser).newPage()
                for (let iterator of list) {
                    const level: any = (await VideoLevel.findOne({ where: { name: iterator._level_content } }))?.toJSON()

                    await tmp_page.goto(new URL(iterator._href ?? '', DOMAIN).toString())

                    const lead: string = <string>await tmp_page.$eval('body > div.section1 > div.clearfix.warp > div.fr.video-describe > div.video-lead > p', (e) => {
                        return e.textContent?.trim()
                    })

                    iterator.videoLevelId = level.id;
                    iterator.videoSubjectId = subject.id
                    // 如果可行，应该使用 new URL 拼接
                    iterator.banner = new URL(iterator.banner ?? '', DOMAIN).toString()
                    iterator.lead = lead;
                    delete iterator._level_content
                    delete iterator._href
                }
                await tmp_page.close()

                await VideoList.bulkCreate(list);
            }
            // 子页数量最大 20
            const MAX_COUNT = 20;
            if ((await (await browser).pages()).length > MAX_COUNT) {
                await save()
            } else {
                save()
            }
            // 尝试爬取下一页
            try {
                const next_url = await page.$eval('body > section.video-list > div > div > a:last-child', (e) => {
                    return e.getAttribute('href')
                })

                if (next_url == null) {
                    break
                } else {
                    await page.goto(next_url)
                }
            } catch (e) {
                break;
            }

        } while (true)

    }

}
/** 爬取难度 */
async function spiderLevel() {
    const page = await (await browser).newPage()

    await page.goto('http://video.mobiletrain.org/courses/')

    const levels = (await page.$$eval('body > section.filter-nav > div > div.clearfix.filter-type2 > div.fl.clearfix > a.fl', (elements) => {
        return elements.map(e => e.textContent?.trim())
    })).map(e => ({
        name: e
    }))
    await VideoLevel.destroy({ truncate: true })
    await VideoLevel.bulkCreate(levels)
}
/** 爬取学科 */
async function spiderSubject() {
    const page = await (await browser).newPage()

    await page.goto('http://video.mobiletrain.org/courses/')

    const subjects = (await page.$$eval('body > section.filter-nav > div > div.clearfix.filter-type1 > div.fl.clearfix > a.fl', (elements) => {
        return elements.map(e => e.textContent?.trim())
    })).map(e => ({
        name: e
    }))
    await VideoSubject.destroy({ truncate: true })
    await VideoSubject.bulkCreate(subjects)
}