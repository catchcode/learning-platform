/*
 * @Descripttion: 首页
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-04-30 20:52:46
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-05-08 19:08:40
 */
import Router from 'koa-router';
import Koa from 'koa'
const router = new Router();


// 模型
import { News } from '../model/News'
import { VideoList } from '../model/Video';
// 首页
router.get(['/', '/home'], async (ctx: Koa.ParameterizedContext<Koa.DefaultState, Koa.DefaultContext, any>, next: Function) => {
    const newsList = await News.findAll({ limit: 8 });
    const count = await VideoList.count()
    const limit = 4;
    const offset = Math.floor(count * Math.random() - limit);
    const videoList = await VideoList.findAll({ offset, limit })
    ctx.render('home/index', { newsList, videoList })
    await next();
})

module.exports = router;