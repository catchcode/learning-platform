/*
 * @Descripttion: 个人中心页面
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-04-30 20:52:57
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-05-01 07:50:02
 */
import Router from 'koa-router';
import Koa from 'koa'
const router = new Router();

// 模型
import { User, UserRole, UserStatus, Profession } from '../model/User'

import online from '../middleware/online'

/** 个人中心 */
router.get('/my', online, async (ctx: Koa.ParameterizedContext<Koa.DefaultState, Koa.DefaultContext, any>) => {
    const professions = (await Profession.findAll({ limit: 100 })).map(e => e.toJSON())
    await ctx.render('subpage/my', { professions })
})
/** 修改 */
router.post('/my', online, async (ctx: Koa.ParameterizedContext<Koa.DefaultState, Koa.DefaultContext, any>) => {
    try {
        let user = (await User.findOne({ where: { id: ctx.session!.user.id, password: ctx.request.body.password }, include: [UserRole, UserStatus, Profession] }))
        if (user == null) {
            throw new Error('密码错误')
        }

        user.set('truename', ctx.request.body.truename)
        user.set('age', ctx.request.body.age)
        user.set('studentId', ctx.request.body.studentId)
        user.set('professionId', ctx.request.body.professionId)

        await user.save();
        // 更新session
        ctx.session!.user = user?.toJSON()
        ctx.redirect('/my')
    } catch (e) {
        const professions = (await Profession.findAll({ limit: 100 })).map(e => e.toJSON())
        await ctx.render('subpage/my', { professions, danger: [e.message] })
    }

})

module.exports = router;