/*
 * @Descripttion: 自动引入当前目录的所有router文件
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-04-30 19:34:11
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-04-30 21:47:42
 */
import Koa from 'koa'
import Router from 'koa-router'
import fs from 'fs'
import path from 'path'
export default function (app: Koa) {
    const dir = fs.opendirSync(__dirname)
    let name = null;
    while (name = dir.readSync()?.name) {
        if (name != 'index.js' && name != 'other.js') {
            const router: Router = require(path.join(__dirname, name));
            try {
                app.use(router.routes()).use(router.allowedMethods())
            } catch (e) {
                console.error(`加载路由 ${name} 时抛出：${e.message}`)
            }
        }
    }

    const other: Router = require('./other');
    app.use(other.routes()).use(other.allowedMethods())
}