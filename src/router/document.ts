/*
 * @Descripttion: 课程文档页面
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-04-30 20:52:26
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-05-02 08:49:38
 */
import Router from 'koa-router';
import Koa from 'koa'
const router = new Router();

import { Chapter, Course } from '../model/Document';

import { studyRecord } from '../middleware/studyRecord'

// 课程（菜鸟教程）
router.get('/course-information', async (ctx: Koa.ParameterizedContext<Koa.DefaultState, Koa.DefaultContext, any>) => {

    const course = (await Course.findAll()).map(e => e.toJSON())
    await ctx.render('subpage/courseInformation', {
        course,
    })
}, studyRecord)

/** 章节 */
router.get('/chapters', async (ctx: Koa.ParameterizedContext<Koa.DefaultState, Koa.DefaultContext, any>) => {
    try {

        const { courseID, chapterID } = ctx.query
        const where = { courseID, id: chapterID }
        if (!chapterID) {
            delete where.id
        }
        const chapter = (await Chapter.findAll({ where: { courseID: courseID } })).map(e => e.toJSON())
        const { content } = <any>(await Chapter.findOne({ where }))?.toJSON()
        const { name } = <any>(await Course.findOne({ where: { id: courseID } }))?.toJSON()
        await ctx.render('subpage/chapters', {
            list: chapter,
            content: content,
            title: name,
            courseID: courseID
        })
    } catch (e) {
        ctx.redirect('course-information')
    }
}), studyRecord

module.exports = router;