/*
 * @Descripttion: 视频页面
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-04-30 20:53:22
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-05-01 12:54:21
 */
import Router from 'koa-router';
import Koa from 'koa'
const router = new Router();

// 模型
import { VideoSubject, VideoList, VideoLevel, VideoSection } from '../model/Video';


/** 视频列表 */
router.get('/videolist', async (ctx: Koa.ParameterizedContext<Koa.DefaultState, Koa.DefaultContext, any>) => {
    const limit = 12;
    const page = parseInt(<string>ctx.request.query.page) || 1

    const currentPage = page - 1

    const offset = currentPage * limit;

    let levels: any[] = (await VideoLevel.findAll()).map(e => e.toJSON());

    const { subjectId, levelId }: { subjectId: string | undefined, levelId: string | undefined } = <any>ctx.query;
    const where = { videolevelId: levelId, videoSubjectId: subjectId }
    if (levelId == undefined || levelId == '' || levels.find(e => e.id == levelId.trim()) == undefined) {
        delete where.videolevelId
    }
    if (subjectId == undefined || subjectId == '' || typeof parseInt(subjectId) != 'number') {
        delete where.videoSubjectId
    }

    const count = await VideoList.count({ where })

    const countPage = Math.ceil(count / limit)




    // 学科
    let subjects: any[] = (await VideoSubject.findAll()).map(e => e.toJSON())
    // 这个学科的视频
    const videos = (await VideoList.findAll({ where, include: [VideoLevel, VideoSubject], limit, offset })).map(e => e.toJSON())

    await ctx.render('subpage/videolist', {
        subjects,
        videos,
        subjectId,
        levelId,
        levels,
        countPage,
        page
    })
})
/** 视频 */
router.get('/video', async (ctx: Koa.ParameterizedContext<Koa.DefaultState, Koa.DefaultContext, any>, next: Function) => {
    const { videoListId, videoSectionId }: { videoListId: string | undefined, videoSectionId: string | undefined } = <any>ctx.query
    if (videoListId == undefined || typeof parseInt(videoListId) != 'number') {
        next();
        return;
    }

    // 这个学科的所有视频
    const videoSections: any[] = (await VideoSection.findAll({ where: { videoListId: videoListId }, include: VideoList })).map(e => e.toJSON())
    // 当前播放的视频
    const videoSection: any = !videoSectionId ?
        videoSections[0] :
        (await VideoSection.findOne({ where: { id: videoSectionId }, include: VideoList }))?.toJSON()
    // 相关推荐
    const videoSubjectId = videoSections[0].video_list['videoSubjectId'] ?? undefined
    const recommends = (await VideoList.findAll({ where: { videoSubjectId }, limit: 8 })).map(e => e.toJSON())

    await ctx.render('common/video', {
        videoSections,
        videoSection,
        videoListId,
        recommends
    })
})

module.exports = router;
