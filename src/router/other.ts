/*
 * @Descripttion: 一些其他页面
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-04-30 20:53:09
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-04-30 23:40:41
 */
import Router from 'koa-router';
import Koa from 'koa'
const router = new Router();

// 模型
import { User, UserRole, UserStatus, Profession } from '../model/User'


// 中间件
import online from '../middleware/online';

// other
import fs from 'fs'
import path from 'path'




// 登录
router.post('/login', async (ctx: Koa.ParameterizedContext<Koa.DefaultState, Koa.DefaultContext, any>) => {
    try {
        const res = await User.findOne({ where: { tel: ctx.request.body.tel, password: ctx.request.body.password }, include: [UserRole, UserStatus, Profession] })
        if (res == null) { throw new Error() }
        ctx.session!.user = res.toJSON();
        console.log('login info:', ctx.session!.user)
        ctx.render('home/index', { success: ['登录成功'] })
    } catch (e) {
        ctx.render('static/login', { danger: ['登录失败'] })
    }
})
// 退出
router.all('/exit', async (ctx: Koa.ParameterizedContext<Koa.DefaultState, Koa.DefaultContext, any>) => {
    ctx.session!.user = null
    ctx.redirect('/home')

})

// 接受注册
router.post('/register', async (ctx: Koa.ParameterizedContext<Koa.DefaultState, Koa.DefaultContext, any>) => {
    try {
        await User.create(ctx.request.body)
        ctx.redirect('/login')
    } catch (e) {
        console.log(e)
        ctx.render('static/register', { danger: ['注册失败'] })
    }
})

// 提交问卷
router.post('/questionnaire', async (ctx: Koa.ParameterizedContext<Koa.DefaultState, Koa.DefaultContext, any>, next: Function) => {
    console.log(ctx.request.body)
    // 返回提交成功页面
    ctx.redirect('/questionnaire_success')
})

// 静态路由 通过请求路径匹配 static 目录下的文件
router.get('/:path', async function (ctx: Koa.ParameterizedContext<Koa.DefaultState, Koa.DefaultContext, any>, next: Function) {
    // @ts-ignore
    if (fs.existsSync(path.resolve('src', 'views', 'static', ctx.request.params.path + '.art'))) {
        // @ts-ignore
        await ctx.render(`static/${ctx.request.params.path}`)
    } else {
        await next();
    }
})


module.exports = router;