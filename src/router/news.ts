/*
 * @Descripttion: 新闻页面
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-04-30 20:53:03
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-04-30 20:53:08
 */
import Router from 'koa-router';
import Koa from 'koa'
const router = new Router();

// 模型
import { News } from '../model/News'


/** 通知公告 */
router.get('/news', async (ctx: Koa.ParameterizedContext<Koa.DefaultState, Koa.DefaultContext, any>) => {
    if (!!ctx.query.id) {
        const news = await News.findOne({ where: { id: ctx.query.id } });
        await ctx.render('subpage/news_content', { news })
    } else {
        const list = await News.findAll({ limit: 20 });
        await ctx.render('subpage/news', { list })
    }
})

module.exports = router;