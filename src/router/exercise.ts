/*
 * @Descripttion: 课程练习页面
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-04-30 20:52:38
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-05-01 18:10:40
 */
import Router from 'koa-router';
import Koa from 'koa'
const router = new Router();

// 模型
import { ExerciseList, ExerciseOption, ExerciseTopic } from '../model/Exercise';
import { StudyRecord } from '../model/Study';

// 中间件
import online from '../middleware/online'

/** 课程练习 */
router.get('/exerciselist', async (ctx: Koa.ParameterizedContext<Koa.DefaultState, Koa.DefaultContext, any>) => {
    const list = (await ExerciseList.findAll({ include: { model: ExerciseTopic, include: [ExerciseOption] } })).map(e => e.toJSON())

    await ctx.render('subpage/exerciselist', { list })
})
router.get('/exercise', online, async (ctx: Koa.ParameterizedContext<Koa.DefaultState, Koa.DefaultContext, any>) => {
    try {
        if (!!ctx.params.id) { throw new Error() }
        const exercise = (await ExerciseList.findOne({ where: { id: ctx.query.id }, include: { model: ExerciseTopic, include: [ExerciseOption] } }))?.toJSON()
        if (exercise == null) { throw new Error() }
        // @ts-ignore
        await ctx.render('subpage/exercise', { exercise })
    } catch (e) {
        ctx.session!.errmsg = e.message
        // 返回列表
        ctx.redirect('/exerciselist')
    }
})
/** 课程练习 */
router.post('/exercise', online, async (ctx: Koa.ParameterizedContext<Koa.DefaultState, Koa.DefaultContext, any>) => {
    try {
        const req = ctx.request.body

        const exercise = (await ExerciseList.findOne({ where: { id: req.id }, include: { model: ExerciseTopic, include: [ExerciseOption] } }))?.toJSON()

        let sum = 0;//总分
        // @ts-ignore
        for (let index: number | string = 0; index < exercise.exercise_topics.length; index++) {
            // @ts-ignore 题目
            const topic: any = exercise.exercise_topics[index];
            // @ts-ignore 这题提交的选项，是个字符串，如果多选的话是一个 string[]
            let req_option: string | string[] = req[topic.id?.toString()];

            // @ts-ignore 多的正确答案的选项 的 ID,其值是一个数组
            const answers: number[] = topic.exercise_options.filter(option => {
                return !!option.answer
                // @ts-ignore
            }).map(e => e.id);

            if (!(req_option instanceof Array)) {
                req_option = [req_option]
            }

            if (req_option.sort().toString() == answers.sort().toString()) {
                sum += parseInt(topic.grade)
            }
        }
        await StudyRecord.create({ ip: ctx.ip, userId: ctx.session!.user.id, grade: sum, studyRecordTypeId: 3, data: JSON.stringify(ctx.request.body) })
        ctx.redirect('/credit')
    } catch (e) {
        ctx.session!.errmsg = '提交失败'
        ctx.redirect('/exerciselist')
    }
})

module.exports = router;