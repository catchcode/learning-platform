// @ts-nocheck
/*
 * @Descripttion: 学习记录页面
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-04-30 20:52:18
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-05-08 21:19:59
 */
import Router from 'koa-router';
import Koa from 'koa'
const router = new Router();

// 模型
import { StudyRecord, StudyRecordType } from '../model/Study';

// 中间件
import online from '../middleware/online'
import { DB } from '../lib/DB';
import { Profession, User } from '../model/User';


/** 学习记录 */
router.get('/credit', online, async (ctx: Koa.ParameterizedContext<Koa.DefaultState, Koa.DefaultContext, any>, next: Function) => {
    const studys = (await StudyRecord.findAll({ where: { userId: ctx.session!.user.id }, limit: 50, include: StudyRecordType, order: [['createdAt', 'DESC']] })).map(e => e.toJSON())
    const user_studys: any[] = (await StudyRecord.findAll({ where: { userId: ctx.session!.user.id } })).map(e => e.toJSON())
    const sum = user_studys.reduce((s, cur) => {
        return s + cur.grade
    }, 0)

    let recommend_user: any = {}
    try {
        // 推荐算法
        const offset = 50;// 偏移系数
        const recommend_user_id = await DB.query("select userId from ( select sum(grade) as sum ,userId from study_records GROUP BY study_records.userId ) as A where A.sum BETWEEN :min and :max and A.userId != :self ORDER BY A.sum limit 0,1", {
            // @ts-ignore
            replacements: { min: sum - offset, max: sum + offset, self: ctx.session.user.id }
        }).then(([[data]]) => {
            // @ts-ignore
            return data.userId || null
        })
        recommend_user = await User.findOne({
            where: {
                id: recommend_user_id
            },
            include: [
                {
                    model: Profession,
                    as: 'profession'
                }
            ]
        }).then(res => {
            if (res == null) {
                throw new Error()
            } else {
                return res.toJSON()
            }
            // 求学习天数
        }).then(res => {
            const learning_days = Date.now() - new Date(res.createdAt).getTime()
            res.learning_days = Math.ceil(learning_days / 1000 / 60 / 60 / 24)
            return res;
        });
    } catch (e) {
        recommend_user = false
    }
    console.log(recommend_user);

    await ctx.render('subpage/credit', { studys, sum, recommend_user })
    await next();
})

module.exports = router;
