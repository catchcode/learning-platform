// @ts-nocheck
/*
 * @Descripttion: 数据可视化
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-05-01 18:11:03
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-05-09 13:30:08
 */
import Router from 'koa-router';
import Koa from 'koa'
import { DB } from '../lib/DB';
import { Log } from '../model/Log';
import { Op } from 'sequelize';
const router = new Router();

router.get("/visual", async (ctx: Koa.ParameterizedContext<Koa.DefaultState, Koa.DefaultContext, any>, next: Function) => {
    // 学习分数
    let percentage_distribution = await DB.query('select sum(study_records.grade) as grade from study_records GROUP BY study_records.userId',)
        // 返回分数数组 [12,20,30] 
        .then(([data]) => {
            // @ts-ignore
            return data.map(e => parseInt(e.grade))
        })
        // 区间
        .then(data => {
            return [
                data.filter(e => e < 100).length,
                data.filter(e => e > 100 && e < 300).length,
                data.filter(e => e > 300 && e < 500).length,
                data.filter(e => e > 500).length,
            ]
        })
        .then(data => {
            return JSON.stringify(data)
        })
    // 活跃时间段
    const active_time = await DB.query("SELECT * from `logs` where `logs`.href like '%/video?%videoSectionId%' or `logs`.href like '%/chapters?courseID=%'").then(([data]) => {
        const res = []
        for (let i = 0; i < 24; i++) {
            // @ts-ignore
            res.push(data.filter(e => new Date(e.createdAt).getHours() == i).length + 3)
        }
        return res;
    }).then(data => {
        return JSON.stringify(data)
    })

    // 问卷调查
    const questionnaire = await Log.findAll({
        where: {
            href: {
                [Op.like]: 'http://%/questionnaire'
            },
            method: 'POST'
        }
    }).then(data => {
        return data.map(e => JSON.parse(e.body))
    })
    console.log(questionnaire);

    const questionnaire_profession = JSON.stringify([
        {
            value: questionnaire.filter(e => e.profession == 'JY').length,
            name: '计算机应用技术'
        },
        {
            name: '物联网',
            value: questionnaire.filter(e => e.profession == 'JY').length,

        }, {
            name: '云计算',
            value: questionnaire.filter(e => e.profession == 'YJS').length,

        }, {
            name: '会计',
            value: questionnaire.filter(e => e.profession == 'HJ').length,
        }
    ])
    const questionnaire_position = JSON.stringify([
        {
            value: questionnaire.filter(e => e.position.indexOf('CEO') > -1).length,
            name: '项目总经理'
        },
        {
            value: questionnaire.filter(e => e.position.indexOf('WM') > -1).length,
            name: '产品主管'
        },
        {
            value: questionnaire.filter(e => e.position.indexOf('UI') > -1).length,
            name: 'UI设计师'
        }, {
            value: questionnaire.filter(e => e.position.indexOf('AD') > -1).length,
            name: '美工'
        }, {
            value: questionnaire.filter(e => e.position.indexOf('WF') > -1).length,
            name: '前端开发工程师'
        }, {
            value: questionnaire.filter(e => e.position.indexOf('YW') > -1).length,
            name: '产品运维工程师'
        }, {
            value: questionnaire.filter(e => e.position.indexOf('FX') > -1).length,
            name: '数据分析师'
        }, {
            value: questionnaire.filter(e => e.position.indexOf('SJ') > -1).length,
            name: '设计师'
        }
    ])
    const questionnaire_learning_style = JSON.stringify([{
        value: questionnaire.filter(e => e.learningStyle.indexOf('video') > -1).length,
        name: "观看视频",
    },
    {
        value: questionnaire.filter(e => e.learningStyle.indexOf('document') > -1).length,
        name: "查阅文档",

    },
    {
        value: questionnaire.filter(e => e.learningStyle.indexOf('topic') > -1).length,
        name: "刷题",

    },
    {
        value: questionnaire.filter(e => e.learningStyle.indexOf('other') > -1).length,
        name: "其他",
    }
    ])
    console.log(questionnaire_profession);
    console.log(questionnaire_learning_style);

    console.log(questionnaire_position);



    ctx.render('subpage/visual', { percentage_distribution, active_time, questionnaire_profession, questionnaire_position, questionnaire_learning_style })
    await next();
})

module.exports = router;