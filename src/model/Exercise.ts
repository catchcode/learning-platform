import { DB } from "../lib/DB"
import { DataTypes } from "sequelize"

// 练习
const ExerciseList = DB.define('exercise_list', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    title: {
        type: DataTypes.TEXT,
        allowNull: false,
        comment: '练习标题',
    },
    level: {
        type: DataTypes.STRING,
        allowNull: false,
        defaultValue: '正常',
        comment: '题目等级',
    },
    sum: {
        type: DataTypes.VIRTUAL,
        comment: '总分',
        get() {
            // @ts-ignore
            return this.exercise_topics.reduce((sum: number, cur) => {
                return parseInt(cur.toJSON().grade) + sum
            }, 0)
        }
    }
})

// 题目
const ExerciseTopic = DB.define('exercise_topic', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    title: {
        type: DataTypes.TEXT,
        allowNull: false,
        comment: '题目标题'
    },
    grade: {
        type: DataTypes.INTEGER,
        allowNull: false,
        comment: '分数'
    },
    answerCount: {
        type: DataTypes.VIRTUAL,
        get() {
            // @ts-ignore
            return this.options ? this.options.filter(e => !!e.answer).length : 0;
        },
        comment: "正确答案的数量"
    }
})

// 选项
const ExerciseOption = DB.define('exercise_option', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    content: {
        type: DataTypes.TEXT,
        allowNull: false,
        comment: '选项内容'
    },
    answer: {
        type: DataTypes.TINYINT,
        allowNull: false,
        comment: '答案，正确是1，错误是0'
    }
})

ExerciseTopic.belongsTo(ExerciseList)
ExerciseList.hasMany(ExerciseTopic)

ExerciseOption.belongsTo(ExerciseTopic)
ExerciseTopic.hasMany(ExerciseOption)

export { ExerciseTopic, ExerciseOption, ExerciseList }