import { DB } from "../lib/DB"
import { DataTypes } from "sequelize"

const StudyRecord = DB.define('study_record', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    ip: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
            isIP: true
        },
        comment: '客户IP地址'
    },
    grade: {
        type: DataTypes.INTEGER,
        comment: '分数'
    },
    createLocaleTime: {
        type: DataTypes.VIRTUAL,
        get() {
            // @ts-ignore
            return new Date(this.createdAt).toLocaleString()
        }
    }
})

const StudyRecordType = DB.define('study_record_type', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false,
        comment: '类型的名字'
    }
});
StudyRecordType.hasMany(StudyRecord, {})
StudyRecord.belongsTo(StudyRecordType)
import { User } from './User'
User.hasMany(StudyRecord)
StudyRecord.belongsTo(User)


export { StudyRecord, StudyRecordType }