import { DB } from "../lib/DB"
import { DataTypes } from "sequelize"

// 定义一个栏目的模型
const Course = DB.define("course", {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    url: {
        type: DataTypes.STRING,
        allowNull: false
    },
    img: {
        type: DataTypes.TEXT,
        allowNull: false
    },
    description: {
        type: DataTypes.STRING,
        allowNull: false
    }
})


// 章节
const Chapter = DB.define("chapter", {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    title: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    // 菜鸟教程URL
    url: {
        type: DataTypes.STRING,
        allowNull: false
    },
    content: {
        type: DataTypes.TEXT({ length: 'medium' }),
        allowNull: false
    },
    courseID: {
        type: DataTypes.INTEGER
    }
})

Course.hasMany(Chapter, { foreignKey: "courseID" })

export { Course, Chapter }