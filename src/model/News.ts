import { DB } from "../lib/DB"
import { DataTypes } from "sequelize"

const News = DB.define('news', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    content: {
        type: DataTypes.TEXT,
        allowNull: false,
        comment: '通知内容，html格式'
    },
    title: {
        type: DataTypes.STRING,
        allowNull: false,
        comment: '通知标题'
    },
    createLocaleTime: {
        type: DataTypes.VIRTUAL,
        get() {
            // @ts-ignore
            return new Date(this.createdAt).toLocaleString()
        },
        comment: '虚拟字段，返回格式化后的时间'
    }
})
export { News }