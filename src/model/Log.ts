import { DB } from "../lib/DB"
import { DataTypes } from "sequelize"
import { User } from './User'

export const Log = DB.define('log', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    ms: {
        type: DataTypes.INTEGER,
        allowNull: false,
        comment: '处理这个请求花费的时间（毫秒）'
    },
    ip: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
            isIP: true
        },
        comment: '客户IP地址'
    },
    method: {
        type: DataTypes.ENUM('GET', 'POST', 'HEAD', 'PUT', 'DELETE', 'CONNECT', 'OPTIONS', 'TRACE'),
        allowNull: false,
        comment: "请求方法"
    },
    href: {
        type: DataTypes.TEXT,
        allowNull: false,
        comment: '获取完整的请求URL，包括 protocol，host 和 url。',
    },
    query: {
        type: DataTypes.TEXT,
        comment: "获取解析的查询字符串, 当没有查询字符串时，返回一个空对象。[json]"
    },
    body: {
        type: DataTypes.TEXT,
        comment: "请求体[json]"
    },
    header: {
        type: DataTypes.TEXT,
        comment: "请求头对象[json]"
    },
    raw_request: {
        type: DataTypes.TEXT,
        comment: "未处理的请求对象[json]",
    },
    raw_response: {
        type: DataTypes.TEXT,
        comment: "未处理的响应对象[json]"
    }
})

Log.belongsTo(User)
User.hasMany(Log)