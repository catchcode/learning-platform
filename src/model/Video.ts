import { DataTypes } from "sequelize"
import { DB } from '../lib/DB'

// 学科
export const VideoSubject = DB.define("video_subject", {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false,
        comment: '学科名字'
    },
})

// 难度
export const VideoLevel = DB.define("video_level", {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        comment: '主键'
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false,
        comment: '难度名称'
    }
})

// 视频列表
export const VideoList = DB.define('video_list', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    title: {
        type: DataTypes.STRING,
        allowNull: false,
        comment: '视频标题'
    },
    banner: {
        type: DataTypes.STRING,
        allowNull: false,
        comment: '视频主要展示图片'
    },
    num: {
        type: DataTypes.INTEGER,
        allowNull: false,
        comment: '学习人数'
    },
    lead: {
        type: DataTypes.TEXT,
        allowNull: false,
        defaultValue: '无',
        comment: '下载本章视频你能学到什么？'
    }
})

// 视频（视频章节）
export const VideoSection = DB.define("video_section", {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false,
        comment: '章节名字'
    },
    url: {
        type: DataTypes.STRING,
        allowNull: false,
        comment: '视频播放地址'
    },
})

VideoSubject.hasMany(VideoList)
VideoList.belongsTo(VideoSubject)

VideoLevel.hasMany(VideoList)
VideoList.belongsTo(VideoLevel)

VideoList.hasMany(VideoSection)
VideoSection.belongsTo(VideoList);



