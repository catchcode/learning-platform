import { DB } from "../lib/DB"
import { DataTypes } from "sequelize"

// 用户
export const User = DB.define('user', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        comment: '主键',
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false,
        comment: '用户名',
        validate: {
            notEmpty: true
        }
    },
    truename: {
        type: DataTypes.STRING,
        allowNull: true,
        comment: '真实姓名',
        validate: {
            notEmpty: true
        }
    },
    avatar: {
        type: DataTypes.STRING,
        allowNull: true,
        comment: '头像地址',
    },
    email: {
        type: DataTypes.STRING,
        allowNull: false,
        comment: '电子邮箱',
        validate: {
            isEmail: true
        }
    },
    tel: {
        type: DataTypes.STRING,
        allowNull: false,
        comment: '手机号',
        validate: {
            len: [6, 20]
        }
    },
    password: {
        type: DataTypes.STRING,
        allowNull: false,
        comment: '密码',
        validate: {
            len: [6, 20]
        }
    },
    studentId: {
        type: DataTypes.STRING,
        comment: '学号'
    },
    age: {
        type: DataTypes.TINYINT,
        comment: '年龄'
    },
})
// 状态
export const UserStatus = DB.define('user_status', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        comment: '主键',
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false,
        comment: '状态名字'
    }
})
// 角色
export const UserRole = DB.define('user_role', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        comment: '主键',
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false,
        comment: '角色名字'
    }
});
// 专业
export const Profession = DB.define('profession', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        comment: '主键',
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false,
        comment: '专业名称'
    }
})

User.belongsTo(UserStatus)
UserStatus.hasMany(User, { foreignKey: { defaultValue: 1 }, onDelete: 'SET NULL' })
User.belongsTo(UserRole)
UserRole.hasMany(User, { foreignKey: { defaultValue: 1 }, onDelete: 'SET NULL' })
User.belongsTo(Profession);
Profession.hasMany(User)


